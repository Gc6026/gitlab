# frozen_string_literal: true

module GitlabSubscriptions
  module TrialsHelper
    TRIAL_ONBOARDING_SOURCE_URLS = %w[about.gitlab.com docs.gitlab.com learn.gitlab.com].freeze

    def create_lead_form_data(eligible_namespaces)
      submit_path = trials_path(
        step: GitlabSubscriptions::Trials::CreateService::LEAD,
        **params.permit(:namespace_id).merge(::Onboarding::StatusPresenter.glm_tracking_attributes(params))
      )

      _lead_form_data(eligible_namespaces).merge(submit_path: submit_path)
    end

    def create_duo_pro_lead_form_data(eligible_namespaces)
      submit_path = trials_duo_pro_path(
        step: GitlabSubscriptions::Trials::CreateDuoProService::LEAD,
        namespace_id: params[:namespace_id]
      )

      _lead_form_data(eligible_namespaces).merge(submit_path: submit_path)
    end

    def create_duo_enterprise_lead_form_data(eligible_namespaces)
      submit_path = trials_duo_enterprise_path(
        step: GitlabSubscriptions::Trials::CreateDuoEnterpriseService::LEAD,
        namespace_id: params[:namespace_id]
      )

      _lead_form_data(eligible_namespaces).merge(submit_path: submit_path)
    end

    def should_ask_company_question?
      TRIAL_ONBOARDING_SOURCE_URLS.exclude?(::Onboarding::StatusPresenter.glm_tracking_attributes(params)[:glm_source])
    end

    def trial_namespace_selector_data(namespaces, namespace_create_errors)
      namespace_selector_data(namespace_create_errors).merge(
        any_trial_eligible_namespaces: namespaces.any?.to_s,
        items: namespace_options_for_listbox(namespaces).to_json
      )
    end

    def duo_trial_namespace_selector_data(namespaces, namespace_create_errors)
      namespace_selector_data(namespace_create_errors).merge(
        any_trial_eligible_namespaces: namespaces.any?.to_s,
        items: current_namespaces_for_selector(namespaces).to_json
      )
    end

    def glm_source
      ::Gitlab.config.gitlab.host
    end

    def trial_selection_intro_text(namespaces)
      if namespaces.any?
        s_('Trials|You can apply your trial of Ultimate with GitLab Duo Enterprise to a group.')
      else
        s_('Trials|Create a new group and start your trial of Ultimate with GitLab Duo Enterprise.')
      end
    end

    def show_tier_badge_for_new_trial?(namespace, user)
      ::Gitlab::Saas.feature_available?(:subscriptions_trials) &&
        !namespace.paid? &&
        namespace.private? &&
        namespace.never_had_trial? &&
        can?(user, :read_billing, namespace)
    end

    def namespace_options_for_listbox(namespaces)
      group_options = current_namespaces_for_selector(namespaces)
      options = [
        {
          text: _('New'),
          options: [
            {
              text: _('Create group'),
              value: '0'
            }
          ]
        }
      ]

      options.push(text: _('Groups'), options: group_options) unless group_options.empty?

      options
    end

    def trial_form_errors_message(result)
      unless result.reason == GitlabSubscriptions::Trials::BaseApplyTrialService::GENERIC_TRIAL_ERROR
        return result.errors.to_sentence
      end

      support_link_url = 'https://support.gitlab.com/hc/en-us'
      support_link = link_to('', support_link_url, target: '_blank',
        rel: 'noopener noreferrer')
      safe_format(
        _('Please try again or reach out to %{support_link_start}GitLab Support%{support_link_end}.'),
        tag_pair(support_link, :support_link_start, :support_link_end)
      )
    end

    private

    def trial_submit_text(eligible_namespaces)
      if GitlabSubscriptions::Trials.single_eligible_namespace?(eligible_namespaces)
        s_('Trial|Activate my trial')
      else
        s_('Trial|Continue')
      end
    end

    def current_namespaces_for_selector(namespaces)
      namespaces.map { |n| { text: n.name, value: n.id.to_s } }
    end

    def _lead_form_data(eligible_namespaces)
      {
        first_name: current_user.first_name,
        last_name: current_user.last_name,
        email_domain: current_user.email_domain,
        company_name: current_user.organization,
        submit_button_text: trial_submit_text(eligible_namespaces)
      }.merge(
        params.permit(
          :first_name, :last_name, :company_name, :company_size, :phone_number, :country, :state
        ).to_h.symbolize_keys
      )
    end

    def namespace_selector_data(namespace_create_errors)
      {
        new_group_name: params[:new_group_name],
        # This may allow through an unprivileged submission of trial since we don't validate access on the passed in
        # namespace_id.
        # That is ok since we validate this on submission.
        initial_value: params[:namespace_id],
        namespace_create_errors: namespace_create_errors
      }
    end
  end
end
